package com.example.todo

enum class State {
    PENDING,
    FINISHED
}