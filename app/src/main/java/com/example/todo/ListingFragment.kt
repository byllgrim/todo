package com.example.todo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListingFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_listing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize "recycler view"
        val recycler = view.findViewById<RecyclerView>(R.id.myrv)
        val dataManager = (activity as MainActivity).dataManager
        val adapter = ListingAdapter(dataManager) { position -> rowItemClicked(position) }
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(this.context)

        // Set action for the "floating action button"
        view.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener {
            findNavController().navigate(R.id.action_listingFragment_to_addnoteFragment)
        }
    }

    private fun rowItemClicked(position: Int) {
        val args = Bundle()
        args.putInt("position", position)
        findNavController().navigate(R.id.action_listingFragment_to_edititemFragment, args)
    }
}