package com.example.todo

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton

class EdititemFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edititem, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle: Bundle? = this.arguments
        val position = bundle?.getInt("position")

        // Set text of the text field
        val tv = view.findViewById<TextView>(R.id.textField) as TextView
        if (position != null) {
            tv.text = (activity as MainActivity).dataManager.getTextAt(position)
        }

        // Set action for complete button
        view.findViewById<MaterialButton>(R.id.markButton).setOnClickListener {
            if (position != null) {
                (activity as MainActivity).dataManager.setStateAt(State.FINISHED, position)
                (activity as MainActivity).dataManager.saveToFile()
                findNavController().popBackStack()
            }
        }

        // Set action for save button
        view.findViewById<MaterialButton>(R.id.saveButton).setOnClickListener {
            if (position != null) {
                val text = tv.text.toString()
                (activity as MainActivity).dataManager.setTextAt(text, position)
                (activity as MainActivity).dataManager.saveToFile()
            }
        }

        // Set action for delete button
        view.findViewById<MaterialButton>(R.id.deleteButton).setOnClickListener {
            if (position != null) {
                (activity as MainActivity).dataManager.deleteItem(position)
                findNavController().popBackStack()
            }
        }

        // Set action for return button
        view.findViewById<MaterialButton>(R.id.returnButton).setOnClickListener {
            findNavController().popBackStack()
        }
    }
}