package com.example.todo

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

class DataManager(appContext: Context) {
    private val filename = "myfile"
    private val context: Context = appContext
    private lateinit var dataList: JSONArray

    init {
        // Ensure save file exists
        val filesDir = context.filesDir.absolutePath
        val file = File(filesDir, filename)
        if (!file.exists()) {
            file.createNewFile()
        }

        // Load stored data (or start afresh if no file found)
        context.openFileInput(filename).use {
            val data = ByteArray(it.available())
            it.read(data)
            val dataString = String(data)
            if (!dataString.isNullOrEmpty())
                dataList = JSONArray(dataString)
        }
        if (!this::dataList.isInitialized) {
            dataList = JSONArray()
        }
    }

    fun saveToFile() {
        context.openFileOutput(filename, Context.MODE_PRIVATE).use {
            it.write(dataList.toString().toByteArray())
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun deleteItem(position: Int) {
        dataList.remove(position)
        saveToFile()
    }

    fun addItem(text: String) {
        val obj = JSONObject()
        obj.put("state", State.PENDING)
        obj.put("text", text)
        dataList.put(obj)
    }

    fun getTextAt(position: Int): String {
        val str: String = dataList.optString(position)
        val obj: JSONObject = JSONObject(str)
        return obj.getString("text")
    }

    fun setTextAt(text: String, position: Int) {
        val str: String = dataList.optString(position)
        val obj: JSONObject = JSONObject(str)
        obj.put("text", text)
        dataList.put(position, obj)
    }

    fun getStateAt(position: Int): State {
        val str: String = dataList.optString(position)
        val obj: JSONObject = JSONObject(str)
        val text = obj.getString("state")
        return State.valueOf(text)
    }

    fun setStateAt(state: State, position: Int) {
        val str: String = dataList.optString(position)
        val obj: JSONObject = JSONObject(str)
        obj.put("state", state)
        dataList.put(position, obj)
    }

    fun getItemCount(): Int {
        return dataList.length()
    }

    override fun toString(): String {
        var text = ""
        for (i in 0 until getItemCount()) {
            text += "· " + getTextAt(i) + "\n"
        }
        return text
    }
}