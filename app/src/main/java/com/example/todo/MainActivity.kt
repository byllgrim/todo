package com.example.todo

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var dataManager: DataManager  // TODO make private

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        // Do all the default init shit
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Initialize the data manager
        dataManager = DataManager(applicationContext)

        // Schedule reminder notifications for every hour
        val timerTask = NotificationUpdater(this)
        Timer().scheduleAtFixedRate(timerTask, 0, 1000 * 60 * 60)
    }

    class NotificationUpdater(appContext: Context) : TimerTask() {
        private val context: Context = appContext

        override fun run() {
            val channelId = "my notification channel id"
            val notificationText = (context as MainActivity).dataManager.toString()
            val notification = NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(android.R.drawable.alert_dark_frame)
                    .setContentText(notificationText)
                    .build()
            val channel = NotificationChannel(
                    channelId,
                    "my notification channel name",
                    NotificationManager.IMPORTANCE_DEFAULT
            )
            val notificationManager = NotificationManagerCompat.from(context)

            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(0, notification)
        }
    }
}