package com.example.todo

import android.graphics.Paint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView

class ListingAdapter(
        private val dataManager: DataManager,
        private val clickListener: (Int) -> (Unit)
) : RecyclerView.Adapter<ListingAdapter.MyViewHolder>() {

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rowItem = view.findViewById<LinearLayout>(R.id.rowItem)
        val textView = view.findViewById<TextView>(R.id.rowTextView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        var rowView = inflater.inflate(R.layout.row_item, parent, false)
        return MyViewHolder(rowView)
    }

    override fun getItemCount() = dataManager.getItemCount()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // Set textView data from main list
        holder.textView.text = dataManager.getTextAt(position)

        // Indicate finished tasks
        if (dataManager.getStateAt(position) == State.FINISHED) {
            holder.textView.paintFlags = (holder.textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG)
            holder.textView.isEnabled = false
        }

        // Specify on-click listener
        holder.rowItem.setOnClickListener { clickListener(position) }
    }
}