package com.example.todo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.button.MaterialButton

class AddnoteFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_addnote, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set action for the submission button
        view.findViewById<MaterialButton>(R.id.addButton).setOnClickListener {
            // Add a new entry to the list
            val textView = view.findViewById<TextView>(R.id.textInput)
            val text = textView.text.toString()
            (activity as MainActivity).dataManager.addItem(text)

            // Save state to file
            (activity as MainActivity).dataManager.saveToFile()

            // Go back to other fragment
            findNavController().popBackStack()
        }

        // Set action for cancel button
        view.findViewById<MaterialButton>(R.id.cancelButton).setOnClickListener {
            findNavController().popBackStack()
        }

        // Show keyboard
        val textView = view.findViewById<TextView>(R.id.textInput)
        if (textView.requestFocus()) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(textView, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}